const express = require('express');
const bodyParser = require('body-parser');
const session = require('express-session');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const flash = require('connect-flash');
const mongoose = require('mongoose'); // Assuming you're using MongoDB with Mongoose
const app = express();
const port = process.env.PORT || 3000;

// Middleware
app.use(bodyParser.urlencoded({ extended: true }));
app.use(session({
  secret: 'your-secret-key', // Replace with a secret key
  resave: false,
  saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

// MongoDB setup (using Mongoose)
mongoose.connect('mongodb://localhost/your-database-name', { useNewUrlParser: true });

// Your database schema and model setup go here...

// Passport.js setup
const User = require('./models/user'); // Assuming you have a User model
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());
// Add these routes after your middleware setup

// Display the registration form
app.get('/register', (req, res) => {
  res.sendFile(__dirname + '/views/register.html');
});

// User registration form submission
app.post('/register', (req, res) => {
  const { username, password } = req.body;
  // In a real application, you'd create a new user in the database.
  // For this example, we'll use Passport.js's register method.
  User.register(new User({ username }), password, (err, user) => {
    if (err) {
      console.error(err);
      return res.send('Error registering user');
    }
    passport.authenticate('local')(req, res, () => {
      res.redirect('/dashboard'); // Redirect to the dashboard or user profile page
    });
  });
});

// Display the login form
app.get('/login', (req, res) => {
  res.sendFile(__dirname + '/views/login.html');
});

// User login form submission using Passport.js
app.post('/login', passport.authenticate('local', {
  successRedirect: '/dashboard',
  failureRedirect: '/login',
  failureFlash: true
}));

// User dashboard or profile page (requires login)
app.get('/dashboard', (req, res) => {
  if (!req.isAuthenticated()) {
    // Redirect to login if the user is not authenticated
    return res.redirect('/login');
  }
  res.send(`Welcome to your dashboard, ${req.user.username}!`);
});

// Logout route
app.get('/logout', (req, res) => {
  req.logout();
  res.redirect('/login');
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
